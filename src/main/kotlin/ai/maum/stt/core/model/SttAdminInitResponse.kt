package ai.maum.stt.core.model

import kotlinx.serialization.Serializable

@Serializable
data class SttAdminInitResponse(
        val status: Int? = 0,
        val message: String? = "",
        val payload: SttInitData = SttInitData()
)

@Serializable
data class SttInitData(
        val modelList: List<SttModelWithUsers> = mutableListOf()
)

@Serializable
data class SttModelWithUsers(
        val model: SttModel,
        val modelName: String,
        val users: Set<String>
)
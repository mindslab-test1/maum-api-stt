package ai.maum.stt.util

object APIConstants {
    const val REQUEST_USAGE = "Request-Usage"
    const val RESPONSE_USAGE = "Response-Usage"
}
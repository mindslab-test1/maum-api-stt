package ai.maum.stt.core.model

import kotlinx.serialization.Serializable

@Serializable
data class SttModelUpdate(
        val action: String? = "none",
        val modelName: String? = "",
        val modelInfo: SttModel? = null,
        val clientAction: String? = "none",
        val client: String? = null
)
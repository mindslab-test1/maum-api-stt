package ai.maum.stt.boundaries.controller

import ai.maum.stt.boundaries.dto.CnnDetailDto
import ai.maum.stt.boundaries.dto.CnnRequestDto
import ai.maum.stt.core.usecase.CreateCnnSttUsecase
import ai.maum.stt.util.APIConstants
import org.slf4j.LoggerFactory
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.security.oauth2.jwt.JwtDecoder
import org.springframework.security.oauth2.provider.OAuth2Authentication
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails
import org.springframework.validation.BindException
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import javax.servlet.http.HttpServletRequest
import javax.validation.Valid

@RestController
@RequestMapping("/stt")
class SttController (
    private val createCnnSttUsecase: CreateCnnSttUsecase,
    private val jwtDecoder: JwtDecoder
) {
    private val logger = LoggerFactory.getLogger(this.javaClass)

    @PostMapping("/cnn")
    fun cnn(@RequestHeader httpHeaders: HttpHeaders, @RequestParam("file") file: MultipartFile, oAuth2Authentication: OAuth2Authentication): ResponseEntity<String> {
        logger.debug(file.name)
        logger.debug(httpHeaders.getFirst(HttpHeaders.AUTHORIZATION))
        logger.debug(httpHeaders.getFirst("Request-Unique-Key"))
        logger.debug(file.size.toString())

        val clientId = oAuth2Authentication.principal as String
        logger.info(clientId)

        val response = createCnnSttUsecase.execute(file)
        logger.debug(response.length.toString())

        return ResponseEntity.ok()
                .header(APIConstants.REQUEST_USAGE, file.size.toString())
                .header(APIConstants.RESPONSE_USAGE, response.length.toString())
                .body(response)
    }

    @PostMapping("/cnn/detail")
    fun detail(cnnDetailDto: CnnDetailDto, httpServletRequest: HttpServletRequest) {
        logger.info(cnnDetailDto.toString())
    }

    @PostMapping("cnn/simple", consumes = [MediaType.MULTIPART_FORM_DATA_VALUE])
    fun cnnSimple(
            @RequestHeader httpHeaders: HttpHeaders,
            @Valid cnnRequestDto: CnnRequestDto,
            oAuth2Authentication: OAuth2Authentication
    ): Any {
        val clientId = this.extractClientId(oAuth2Authentication)
        return createCnnSttUsecase.doCnnSttSimple(clientId, cnnRequestDto.model!!, cnnRequestDto.file!!)
    }

    @PostMapping("cnn/script")
    fun cnnScript(
            @RequestHeader httpHeaders: HttpHeaders,
            @Valid cnnRequestDto: CnnRequestDto,
            oAuth2Authentication: OAuth2Authentication
    ): Any {
        val clientId = this.extractClientId(oAuth2Authentication)
        return createCnnSttUsecase.doCnnSttScript(clientId, cnnRequestDto.model!!, cnnRequestDto.file!!)
    }

    private fun extractClientId(oAuth2Authentication: OAuth2Authentication): String{
        val tokenValue = (oAuth2Authentication.details as OAuth2AuthenticationDetails).tokenValue
        val jwtMap = jwtDecoder.decode(tokenValue)
        return jwtMap.getClaim<String>("app_id")
    }
}
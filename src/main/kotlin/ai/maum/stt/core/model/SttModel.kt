package ai.maum.stt.core.model

import kotlinx.serialization.Serializable

@Serializable
data class SttModel(
        val host: String,
        val port: Int,
        val sampleRate: Int? = 8000,
        val lang: String? = "ko_KR",
        val open: Boolean? = false
)
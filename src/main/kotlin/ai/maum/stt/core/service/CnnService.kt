package ai.maum.stt.core.service

import ai.maum.stt.boundaries.dto.CnnScriptSegment
import ai.maum.stt.core.model.SttModel
import org.springframework.web.multipart.MultipartFile

interface CnnService {
    fun handleSpeech(file: MultipartFile): String

    fun cnnSimpleRequest(sttModel: SttModel, modelName: String, file: MultipartFile): String
    fun cnnScriptRequest(sttModel: SttModel, modelName: String, file: MultipartFile): List<CnnScriptSegment>
}
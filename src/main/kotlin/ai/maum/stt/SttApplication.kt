package ai.maum.stt

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SttApplication

fun main(args: Array<String>) {
    runApplication<SttApplication>(*args)
}

package ai.maum.stt.infra.service

import ai.maum.stt.core.exception.*
import ai.maum.stt.core.model.SttAdminInitResponse
import ai.maum.stt.core.model.SttModel
import ai.maum.stt.core.model.SttModelRepository
import ai.maum.stt.core.model.SttModelUpdate
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.server.ResponseStatusException

@Service
class SttModelRepositoryImpl(
        sttAdminWebClient: WebClient,
        val sttChannelHandler: SttChannelHandler
): SttModelRepository {
    private val logger = LoggerFactory.getLogger(this.javaClass)

    private val memoryMap = mutableMapOf<String, Pair<SttModel, MutableSet<String>>>()

    init {
        val initRequest = sttAdminWebClient
                .get()
                .uri("/admin/api/init/getInitialData")
                .header("ai-maum-engine", "STT")
                .retrieve()

        val responseRaw = initRequest.bodyToMono(String::class.java)
                .block() ?: throw ApiTransactionalException(
                message = "response from admin is null!"
        )
        logger.debug("${Json.decodeFromString<SttAdminInitResponse>(responseRaw)}")

        val responsePayload = Json.decodeFromString<SttAdminInitResponse>(responseRaw).payload
        responsePayload.modelList.forEach { modelWithUsers ->
            val modelName = modelWithUsers.modelName
            val modelInfo = SttModel(
                    host = modelWithUsers.model.host,
                    port = modelWithUsers.model.port,
                    sampleRate = modelWithUsers.model.sampleRate,
                    open = modelWithUsers.model.open,
                    lang = modelWithUsers.model.lang
            )
            val clientSet = modelWithUsers.users as MutableSet<String>
            memoryMap[modelName] = Pair(first = modelInfo, second = clientSet)
        }

        logger.info("loaded model list from admin server")
        for(mutableEntry in memoryMap) {
            logger.debug(mutableEntry.key)
            logger.debug(mutableEntry.value.toString())
        }
    }

    override fun getModelInfo(clientId: String, model: String): SttModel {
        val modelPair = memoryMap[model] ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "requested model is not present")
        if(!(modelPair.first.open == true || modelPair.second.contains(clientId)))
            throw  ResponseStatusException(HttpStatus.FORBIDDEN, "model not authenticated for client")

        return modelPair.first
    }

    override fun update(sttModelUpdate: SttModelUpdate) {
        when(sttModelUpdate.action){
            "create" -> {
                createAction(sttModelUpdate)
            }
            "update" -> {
                updateAction(sttModelUpdate)
            }
            "delete" -> {
                val removed = memoryMap.remove(sttModelUpdate.modelName)
                logger.info("removed model: ${sttModelUpdate.modelName}")
                logger.debug("model info: $removed")
                sttChannelHandler.removeChannel(sttModelUpdate.modelName)
            }
            "none" -> {
                logger.warn("no action message presented")
            }
            else -> throw MqUnknownActionException(
                    message = "unknown action ${sttModelUpdate.action}",
                    mqObject = sttModelUpdate
            )
        }
    }

    override fun testGetModels(): MutableMap<String, Pair<SttModel, MutableSet<String>>> {
        return this.memoryMap
    }

    fun createAction(sttModelUpdate: SttModelUpdate){
        sttModelUpdate.modelInfo ?: throw MqInsufficientDataException(
                message = "model info not specified in message",
                mqObject = sttModelUpdate
        )
        sttModelUpdate.client ?: throw MqInsufficientDataException(
                message = "model creator(default user) not specified in message",
                mqObject = sttModelUpdate
        )

        val pair = Pair(
                first = sttModelUpdate.modelInfo,
                second = mutableSetOf(sttModelUpdate.client)
        )
        memoryMap[sttModelUpdate.modelName ?: throw MqConCurrencyException(
                message = "null model name presented"
        )] = pair
        logger.info("created model: ${sttModelUpdate.modelName}")
        logger.debug("model info: ${sttModelUpdate.modelInfo}")

    }

    fun updateAction(sttModelUpdate: SttModelUpdate){
        memoryMap[sttModelUpdate.modelName] ?: throw MqConCurrencyException(
                message = "concurrency exception: no known model name '${sttModelUpdate.modelName}', " +
                        "known models: ${memoryMap.keys}"
        )

        val clientSet: MutableSet<String> = LinkedHashSet()
        clientSet.addAll(memoryMap[sttModelUpdate.modelName]?.second ?: throw ApiInternalException(
                message = "could not find client set for model: ${sttModelUpdate.modelName}"
        ))
        val originModel = memoryMap[sttModelUpdate.modelName]?.first ?: throw ApiInternalException(
                message = "could not find origin model info: ${sttModelUpdate.modelName}"
        )

        when (sttModelUpdate.clientAction){
            "add" -> clientSet.add(sttModelUpdate.client ?: throw MqInsufficientDataException(
                    message = "client info not specified in message"
            ))
            "remove" -> clientSet.remove(sttModelUpdate.client ?: throw MqInsufficientDataException(
                    message = "client info not specified in message"
            ))
        }

        memoryMap[sttModelUpdate.modelName ?: throw MqConCurrencyException(
                message = "concurrency exception: no known model name '${sttModelUpdate.modelName}', " +
                        "known models: ${memoryMap.keys}"
        )] = Pair(
                first = sttModelUpdate.modelInfo ?: originModel,
                second = clientSet
        )

        logger.info("updated model: ${sttModelUpdate.modelName}")
        logger.debug("model info: ${sttModelUpdate.modelInfo}")
        logger.debug("client action: ${sttModelUpdate.clientAction}, client: ${sttModelUpdate.client}")

        sttChannelHandler.removeChannel(sttModelUpdate.modelName)
    }
}
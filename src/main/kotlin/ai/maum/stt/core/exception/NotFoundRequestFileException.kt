package ai.maum.stt.core.exception

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
class NotFoundRequestFileException(override val message: String): BaseException("not_found_request_file", message)
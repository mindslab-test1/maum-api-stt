package ai.maum.stt.core.model

interface SttModelRepository {
    fun getModelInfo(clientId: String, model: String): SttModel
    fun update(sttModelUpdate: SttModelUpdate)
    fun testGetModels(): MutableMap<String, Pair<SttModel, MutableSet<String>>>
}
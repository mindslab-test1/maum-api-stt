package ai.maum.stt.boundaries.dto

import org.springframework.web.multipart.MultipartFile

data class CnnDetailDto(
    val lang: String,
    val model: String,
    val sampleRate: Int,
    var file: MultipartFile
)
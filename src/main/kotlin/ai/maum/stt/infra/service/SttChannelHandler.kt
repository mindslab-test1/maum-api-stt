package ai.maum.stt.infra.service

import io.grpc.ManagedChannel

interface SttChannelHandler {
    fun getOrPutChannel(modelName: String, host: String, port: Int): ManagedChannel
    fun removeChannel(modelName: String?): Unit
}
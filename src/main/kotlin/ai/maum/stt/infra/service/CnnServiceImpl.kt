package ai.maum.stt.infra.service

import ai.maum.stt.boundaries.dto.CnnScriptSegment
import ai.maum.stt.core.exception.ApiInternalException
import ai.maum.stt.core.exception.ApiProcessException
import ai.maum.stt.core.model.SttModel
import ai.maum.stt.core.service.CnnService
import com.google.protobuf.ByteString
import io.grpc.ManagedChannelBuilder
import io.grpc.stub.StreamObserver
import maum.brain.w2l.SpeechToTextGrpc
import maum.brain.w2l.W2L
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.server.ResponseStatusException
import java.io.ByteArrayInputStream
import java.io.InputStream
import java.lang.RuntimeException
import java.nio.charset.StandardCharsets
import java.util.*
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit

@Service
class CnnServiceImpl(
        val sttChannelHandler: SttChannelHandler
): CnnService {
    private val cnnGrpcChannel = ManagedChannelBuilder.forTarget("114.108.173.112:15001").usePlaintext().build()
    private val cnnStub = SpeechToTextGrpc.newStub(cnnGrpcChannel)
    private val logger = LoggerFactory.getLogger(this.javaClass)

    init {
        logger.info("cnn stt grpc target url(\"114.108.173.112:15001\")")
    }

    override fun handleSpeech(file: MultipartFile): String {
        val finishLatch = CountDownLatch(1)
        val resultTextBuffer = StringBuffer()

        // set builder
        val cnnBuilder = W2L.Speech.newBuilder()

        val responseObserver = cnnStub.recognize(object : StreamObserver<W2L.Text> {
            override fun onNext(result: W2L.Text?) {
                resultTextBuffer.append(result?.txt ?: "")
                logger.info("Result: \n${result?.txt}")
            }

            override fun onError(t: Throwable?) {
                logger.error("Err from responseObserver in cnn", t)
                finishLatch.countDown()
            }

            override fun onCompleted() {
                logger.info("Finished from responseObserver in cnn")
                finishLatch.countDown()
            }
        })

        val byteArrayInputStream = ByteArrayInputStream(file.bytes)

        var len: Int
        val buffer = ByteArray(1024 * 512) // data unit: kb
        while (byteArrayInputStream.read(buffer).also { len = it } > 0) {
            if (buffer.size > len) { // 파일 마지막의 남은 크기가 기준보다 작으면
                val inputByteString = ByteString.copyFrom(ByteArray(len))
                responseObserver.onNext(cnnBuilder.setBin(inputByteString).build())
            }
            val inputByteString = ByteString.copyFrom(buffer)
            responseObserver.onNext(cnnBuilder.setBin(inputByteString).build())
        }

        responseObserver.onCompleted()
        byteArrayInputStream.close()

        try {
            finishLatch.await(1, TimeUnit.MINUTES)
        } catch (e: InterruptedException) {
            logger.error(e.localizedMessage)
            throw RuntimeException(e)
        }

        val resultText = resultTextBuffer.toString()
        logger.info("[CNN-STT Final] $resultText")
        return resultText
    }

    override fun cnnSimpleRequest(sttModel: SttModel, modelName: String, file: MultipartFile): String {
        val channel = sttChannelHandler.getOrPutChannel(
                modelName, sttModel.host, sttModel.port
        )
        val cnnStub = SpeechToTextGrpc.newStub(channel)
        val finishLatch = CountDownLatch(1)
        val resultTextBuffer = StringBuffer()
        val errorList = mutableListOf<Throwable>()

        val cnnBuilder = W2L.Speech.newBuilder()

        val requestObserver = cnnStub.recognize(object : StreamObserver<W2L.Text> {
            override fun onNext(result: W2L.Text?) {
                resultTextBuffer.append(result?.txt ?: "")
                logger.debug("Result: \n${result?.txt}")
            }

            override fun onError(t: Throwable?) {
                logger.error("Err from responseObserver in cnn", t)
                errorList.add(t?: Exception("unknown"))
                finishLatch.countDown()
            }

            override fun onCompleted() {
                logger.debug("Finished from responseObserver in cnn")
                finishLatch.countDown()
            }
        })

        val byteArrayInputStream = ByteArrayInputStream(file.bytes)

        var len: Int
        val buffer = ByteArray(1024 * 512) // data unit: kb
        while (byteArrayInputStream.read(buffer).also { len = it } > 0) {
            if (buffer.size > len) { // 파일 마지막의 남은 크기가 기준보다 작으면
                val inputByteString = ByteString.copyFrom(buffer.sliceArray(IntRange(0, len - 1)))
                requestObserver.onNext(cnnBuilder.setBin(inputByteString).build())
            }
            else {
                val inputByteString = ByteString.copyFrom(buffer)
                requestObserver.onNext(cnnBuilder.setBin(inputByteString).build())
            }
        }

        requestObserver.onCompleted()
        byteArrayInputStream.close()

        try {
            if(!finishLatch.await(1, TimeUnit.MINUTES))
                throw ApiInternalException(message = "connection timeout")
        } catch (e: InterruptedException) {
            logger.error(e.localizedMessage)
            throw RuntimeException(e)
        }

        if(errorList.isNotEmpty()){
            logger.error(errorList[0].message)
            throw ApiInternalException(message = "internal error has occurred")
        }

        return resultTextBuffer.toString()
    }

    override fun cnnScriptRequest(sttModel: SttModel, modelName: String, file: MultipartFile): List<CnnScriptSegment> {
        val channel = sttChannelHandler.getOrPutChannel(
                modelName, sttModel.host, sttModel.port
        )
        val cnnStub = SpeechToTextGrpc.newStub(channel)
        val finishLatch = CountDownLatch(1)
        val resultList = mutableListOf<W2L.TextSegment?>()
        val errorList = mutableListOf<Throwable>()

        val cnnBuilder = W2L.Speech.newBuilder()

        val requestObserver = cnnStub.streamRecognize(object : StreamObserver<W2L.TextSegment> {
            override fun onNext(result: W2L.TextSegment?) {
                resultList.add(result)
                logger.debug("Result: \n${result?.txt}")
            }

            override fun onError(t: Throwable?) {
                logger.error("Err from responseObserver in cnn", t)
                errorList.add(t?: Exception("unknown"))
                finishLatch.countDown()
            }

            override fun onCompleted() {
                logger.debug("Finished from responseObserver in cnn")
                finishLatch.countDown()
            }
        })
        val fileBytes = file.bytes.copyOfRange(0, file.bytes.size)
        val fileString = String(fileBytes, StandardCharsets.US_ASCII)
        val dataIndex = fileString.indexOf("data")
        val dataLength = (
                java.lang.Byte.toUnsignedInt(fileBytes[dataIndex + 4])
                        .or(java.lang.Byte.toUnsignedInt(fileBytes[dataIndex + 5]).shl(8))
                        .or(java.lang.Byte.toUnsignedInt(fileBytes[dataIndex + 6]).shl(16))
                        .or(java.lang.Byte.toUnsignedInt(fileBytes[dataIndex + 7]).shl(24))
        )

        if(dataLength != fileBytes.size - dataIndex - 8)
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "file byte length does not match metadata length")


        val byteArrayInputStream = ByteArrayInputStream(fileBytes.sliceArray(
                IntRange(dataIndex + 8, dataIndex + dataLength + 7)
        ))

        var len: Int
        val buffer = ByteArray(1024 * 512) // data unit: kb
        while (byteArrayInputStream.read(buffer).also { len = it } > 0) {
            if (buffer.size > len) { // 파일 마지막의 남은 크기가 기준보다 작으면
                val inputByteString = ByteString.copyFrom(buffer.sliceArray(IntRange(0, len - 1)))
                requestObserver.onNext(cnnBuilder.setBin(inputByteString).build())
            }
            else {
                val inputByteString = ByteString.copyFrom(buffer)
                requestObserver.onNext(cnnBuilder.setBin(inputByteString).build())
            }
        }

        requestObserver.onCompleted()
        byteArrayInputStream.close()

        try {
            if(!finishLatch.await(1, TimeUnit.MINUTES))
                throw ApiInternalException(message = "connection timeout")
        } catch (e: InterruptedException) {
            logger.error(e.localizedMessage)
            throw RuntimeException(e)
        }

        if(errorList.isNotEmpty()){
            logger.error(errorList[0].message)
            throw ApiInternalException(message = "internal error has occurred")
        }

        val returnList = mutableListOf<CnnScriptSegment>()
        resultList.forEach { textSegment ->
            if(textSegment != null) returnList.add(CnnScriptSegment(
                    text = "${textSegment.txt} ",
                    startTime = sttModel.sampleRate?.toDouble()?.let { textSegment.start.div(it) },
                    endTime = sttModel.sampleRate?.toDouble()?.let { textSegment.end.div(it) }
            ))
        }

        return returnList
    }

}
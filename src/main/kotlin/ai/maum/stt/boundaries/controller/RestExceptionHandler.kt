package ai.maum.stt.boundaries.controller

import ai.maum.stt.boundaries.dto.SttApiResponseError
import ai.maum.stt.core.exception.BaseException
import org.slf4j.LoggerFactory
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.validation.BindException
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.RestControllerAdvice
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler

@RestControllerAdvice
private class RestExceptionHandler: ResponseEntityExceptionHandler() {

    private val loggers = LoggerFactory.getLogger(RestExceptionHandler::class.java)

    @ExceptionHandler(BaseException::class)
    fun handle(exception: BaseException, request: WebRequest): ResponseEntity<Any> {
        loggers.warn("handleBaseException $exception")
        val sttApiResponseError = SttApiResponseError(exception.code, exception.message)
        return handleExceptionInternal(exception, sttApiResponseError, HttpHeaders(), HttpStatus.BAD_REQUEST, request)
    }

    override fun handleBindException(exception: BindException, headers: HttpHeaders, status: HttpStatus, request: WebRequest): ResponseEntity<Any> {
        loggers.warn("handle Bind Exception $exception")
        return handleExceptionInternal(exception, exception.bindingResult, HttpHeaders(), HttpStatus.BAD_REQUEST, request)
    }
}
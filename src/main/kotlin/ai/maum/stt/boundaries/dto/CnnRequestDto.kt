package ai.maum.stt.boundaries.dto

import org.springframework.web.multipart.MultipartFile
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull

data class CnnRequestDto(
        @field:NotBlank(message = "please enter your model")
        val model: String? = null,
        @field:NotNull(message = "please enter your file")
        var file: MultipartFile? = null
)
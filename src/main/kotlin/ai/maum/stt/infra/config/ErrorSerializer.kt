package ai.maum.stt.infra.config

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.JsonSerializer
import com.fasterxml.jackson.databind.SerializerProvider
import org.slf4j.LoggerFactory
import org.springframework.boot.jackson.JsonComponent
import org.springframework.validation.Errors
import java.io.IOException

@JsonComponent
class ErrorSerializer: JsonSerializer<Errors>() {

    private val logger = LoggerFactory.getLogger(ErrorSerializer::class.java)

    override fun serialize(errors: Errors, gen: JsonGenerator, serializers: SerializerProvider) {
        gen.writeStartArray()
        errors.fieldErrors.forEach {
            try {
                gen.writeStartObject()
                gen.writeStringField("code", it.code)
                gen.writeStringField("message", it.defaultMessage)
                val rejectedValue = it.rejectedValue
                rejectedValue.let {
                    gen.writeStringField("rejectedValue", rejectedValue.toString())
                }
                gen.writeEndObject()
            } catch (exception: IOException) {
                exception.printStackTrace()
                logger.warn(exception.message)
            }
        }

        errors.globalErrors.forEach {
            try {
                gen.writeStartObject()
                gen.writeStringField("code", it.code)
                gen.writeStringField("message", it.defaultMessage)
                gen.writeEndObject()
            } catch (exception: IOException) {
                exception.printStackTrace()
                logger.warn(exception.message)
            }
        }

        gen.writeEndArray()
    }

}
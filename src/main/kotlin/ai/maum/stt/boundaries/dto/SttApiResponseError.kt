package ai.maum.stt.boundaries.dto

data class SttApiResponseError(val code: String, val message: String)
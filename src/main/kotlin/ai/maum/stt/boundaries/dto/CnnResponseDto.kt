package ai.maum.stt.boundaries.dto

data class CnnSimpleResponseDto(
        val status: Int? = 0,
        val message: String? = "Success",
        val payload: CnnSimplePayload? = CnnSimplePayload()
)

data class CnnSimplePayload(
        val resultText: String? = ""
)

data class CnnScriptResponseDto(
        val status: Int? = 0,
        val message: String? = "Success",
        val payload: CnnScriptPayload? = CnnScriptPayload()
)

data class CnnScriptPayload(
        val resultText: String? = "",
        val segments: List<CnnScriptSegment>? = listOf()
)

data class CnnScriptSegment(
        val text: String? = "",
        val startTime: Double? = 0.0,
        val endTime: Double? = 0.0
)
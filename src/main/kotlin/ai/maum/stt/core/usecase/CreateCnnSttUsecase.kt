package ai.maum.stt.core.usecase

import ai.maum.stt.boundaries.dto.CnnScriptPayload
import ai.maum.stt.boundaries.dto.CnnScriptResponseDto
import ai.maum.stt.boundaries.dto.CnnSimplePayload
import ai.maum.stt.boundaries.dto.CnnSimpleResponseDto
import ai.maum.stt.core.exception.NotFoundRequestFileException
import ai.maum.stt.core.model.SttModelRepository
import ai.maum.stt.core.service.CnnService
import ai.maum.stt.infra.aop.PostSaveContents
import ai.maum.stt.util.FileUtils
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import org.springframework.web.multipart.MultipartFile

@Component
class CreateCnnSttUsecase(
    private val cnnService: CnnService,
    private val sttModelRepository: SttModelRepository
) {
    private val logger = LoggerFactory.getLogger(this.javaClass)

    @PostSaveContents
    fun execute(file: MultipartFile): String {
        logger.info("Request upload file info(${file.contentType}, ${file.size} -> ${FileUtils.readableFileSizeUnit(file.size)})")
        file.originalFilename ?: logger.warn("Not found request upload file original name")

        return cnnService.handleSpeech(file)
    }

//    @PostSaveContents
    fun doCnnSttSimple(clientId: String, model: String, file: MultipartFile): Any {
        // TODO: PostsSaveContent annotation에 따른 기능 활성화시 해당 AOP에서 검증 및 thorw 처리
        if (file.size == 0L) {
            throw NotFoundRequestFileException("File is not exist in Request Param")
        }
        val modelInfo = sttModelRepository.getModelInfo(clientId, model)
        val resultText = cnnService.cnnSimpleRequest(sttModel = modelInfo, modelName = model, file = file)
        return CnnSimpleResponseDto(
                payload = CnnSimplePayload(resultText = resultText)
        )
    }

    fun doCnnSttScript(clientId: String, model: String, file: MultipartFile): Any{
        // TODO: PostsSaveContent annotation에 따른 기능 활성화시 해당 AOP에서 검증 및 thorw 처리
        if (file.size == 0L) {
            throw NotFoundRequestFileException("File is not exist in Request Param")
        }
        val modelInfo = sttModelRepository.getModelInfo(clientId, model)
        val resultSegments = cnnService.cnnScriptRequest(sttModel = modelInfo, modelName = model, file = file)
        val resultTextBuffer = StringBuffer()
        resultSegments.forEach { segment ->
            resultTextBuffer.append(segment.text)
        }

        return CnnScriptResponseDto(
                payload = CnnScriptPayload(
                        resultText = resultTextBuffer.toString(),
                        segments = resultSegments
                )
        )
    }
}

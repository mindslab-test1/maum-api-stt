package ai.maum.stt.infra.service

import ai.maum.stt.core.model.SttModelRepository
import ai.maum.stt.core.model.SttModelUpdate
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import org.slf4j.LoggerFactory
import org.springframework.amqp.AmqpRejectAndDontRequeueException
import org.springframework.amqp.rabbit.annotation.RabbitHandler
import org.springframework.amqp.rabbit.annotation.RabbitListener
import org.springframework.stereotype.Service

@Service
@RabbitListener(queues = ["#{autoGenQueue.name}"])
class FanoutMessageReceiver(
        val sttModelRepository: SttModelRepository
) {
    private val logger = LoggerFactory.getLogger(this.javaClass)

    @RabbitHandler
    fun receiveMessage(message: String){
        logger.debug("received: $message")
        val modelUpdate: SttModelUpdate = try {
            Json.decodeFromString(message)
        } catch (e: Exception){
            logger.error(e.toString())
            throw AmqpRejectAndDontRequeueException(e)
        }
        logger.info(modelUpdate.toString())
        sttModelRepository.update(modelUpdate)
    }
}